-- Run the MYSQL/ Maria DB in terminal
mysql -u root -p

-- Show/retrive all the database:
SHOW DATABASES;

-- Create Database:
--Syntax: CREATE DATABASE database_name;
CREATE DATABASE music_store;

--DELETE/DROP A DATABASE:
-- Syntax: DELETE DATABASE database_name;
--Syntax: DROP DATABASE database_name;
DROP DATABASE music_store;

--Select/Use a database;
--Syntax: USE database_name;
USE music_store;

--Create/Add Tables:
-- Syntax: CREATE Table tbl_name(column 1,column2, PRIMARY KEY (id));
CREATE TABLE singers(
id INT, NOT NULL AUTO_INCREMENT,
name VARCHAR(50) NOT NULL,
PRIMARY KEY (id)
);

--showing/retrieving tables.
--Syntax: SHOW TABLES;
SHOW TABLES;

---DELETE/DROP TABLE:
--Syntax: DROP TABLE <TABLE_name>
DROP TABLE singers;


--Create Artist Tables:
CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY(id));


-- Describing /showing the information about the table:
-- Syntax: DESCRIBE table_name;
DESCRIBE artists;

--Create a "RECORDS" table

--table with foreign key:

/*
	CONSTRAINT foreign_key_name
		FOREIGN KEY (COLUMN_NAME)
		REFERENCES tbl_name (id)
		ON UPDATE ACTION (NO ACTION,CASCADE ,SET NULL, SET DEFAULT)
		ON DELETE ACTION (RESTRICT,SET NULL)
*/

CREATE TABLE records(
id INT NOT NULL AUTO_INCREMENT,
album_title VARCHAR(25) NOT NULL,
artist_id INT NOT NULL,
PRIMARY KEY (id),
CONSTRAINT fk_albums_artist_id
	FOREIGN KEY artist_id REFERENCES artists(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);